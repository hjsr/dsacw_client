﻿using System;
using System.Web.Security;
using PHRClientWebRole.PHRTaskService;

namespace PHRClientWebRole
{
    public class TaskDAL
    {
        public static Task[] selectAllTasks()
        {
            TaskServiceClient client = new TaskServiceClient();
            ITaskService service = client.ChannelFactory.CreateChannel();

            IdMessage idUser = new IdMessage();
            idUser.Timestamp = DateTime.Now;
            idUser.Id = (int)Membership.GetUser().ProviderUserKey;

            TaskArrayMessage taskArrayMessage = service.getAllTasks(idUser);

            return taskArrayMessage.TaskArray;
        }

        public static void updateTask(int id, string caption, DateTime date, string description, PriorityType priority, bool done)
        {
            TaskServiceClient client = new TaskServiceClient();
            ITaskService service = client.ChannelFactory.CreateChannel();

            TaskMessage taskMessage = new TaskMessage();
            taskMessage.Timestamp = DateTime.Now;
            taskMessage.Task = new Task();
            taskMessage.Task.Caption = caption;
            taskMessage.Task.Date = date;
            taskMessage.Task.Description = description;
            taskMessage.Task.Id = id;
            taskMessage.Task.Priority = priority;
            taskMessage.Task.Done = done;

            service.editTask(taskMessage);
        }

        public static void insertTask(string caption, DateTime date, string description, PriorityType priority)
        {
            TaskServiceClient client = new TaskServiceClient();
            ITaskService service = client.ChannelFactory.CreateChannel();

            TaskMessage taskMessage = new TaskMessage();
            taskMessage.Timestamp = DateTime.Now;
            taskMessage.Task = new Task();
            taskMessage.Task.Caption = caption;
            taskMessage.Task.Date = date;
            taskMessage.Task.Description = description;
            taskMessage.Task.IdUser = (int)Membership.GetUser().ProviderUserKey;
            taskMessage.Task.Priority = priority;

            service.addTask(taskMessage);
        }

        public static void deleteTask(int id)
        {
            TaskServiceClient client = new TaskServiceClient();
            ITaskService service = client.ChannelFactory.CreateChannel();

            TaskMessage taskMessage = new TaskMessage();
            taskMessage.Timestamp = DateTime.Now;
            taskMessage.Task = new Task();
            taskMessage.Task.Id = id;

            service.removeTask(taskMessage);
        }
    }
}