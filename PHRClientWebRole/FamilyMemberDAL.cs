﻿using System;
using System.Web.Security;
using PHRClientWebRole.PHRFamilyMemberService;

namespace PHRClientWebRole
{
    public class FamilyMemberDAL
    {
        public static Person[] selectAllFamilyMembers()
        {
            FamilyMemberServiceClient client = new FamilyMemberServiceClient();
            IFamilyMemberService service = client.ChannelFactory.CreateChannel();

            IdMessage idUser = new IdMessage();
            idUser.Timestamp = DateTime.Now;
            idUser.Id = (int)Membership.GetUser().ProviderUserKey;
            
            PersonArrayMessage personArrayMessage = service.getAllFamilyMembers(idUser);

            return personArrayMessage.PersonArray;
        }

        public static void updateFamilyMember(int id, string address, DateTime dateOfBirth, string email, Gender gender, string name)
        {
            FamilyMemberServiceClient client = new FamilyMemberServiceClient();
            IFamilyMemberService service = client.ChannelFactory.CreateChannel();

            PersonMessage personMessage = new PersonMessage();
            personMessage.Timestamp = DateTime.Now;
            personMessage.Person = new Person();
            personMessage.Person.Address = address;
            personMessage.Person.DateOfBirth = dateOfBirth;
            personMessage.Person.Email = email;
            personMessage.Person.Gender = gender;
            personMessage.Person.Id = id;
            personMessage.Person.Name = name;

            service.editFamilyMember(personMessage);
        }

        public static void insertFamilyMember(string address, DateTime dateOfBirth, string email, Gender gender, string name)
        {
            FamilyMemberServiceClient client = new FamilyMemberServiceClient();
            IFamilyMemberService service = client.ChannelFactory.CreateChannel();

            PersonMessage personMessage = new PersonMessage();
            personMessage.Timestamp = DateTime.Now;
            personMessage.Person = new Person();
            personMessage.Person.Address = address;
            personMessage.Person.DateOfBirth = dateOfBirth;
            personMessage.Person.Email = email;
            personMessage.Person.Gender = gender;
            personMessage.Person.IdUser = (int)Membership.GetUser().ProviderUserKey;
            personMessage.Person.Name = name;

            service.addFamilyMember(personMessage);
        }

        public static void deleteFamilyMember(int id)
        {
            FamilyMemberServiceClient client = new FamilyMemberServiceClient();
            IFamilyMemberService service = client.ChannelFactory.CreateChannel();

            PersonMessage personMessage = new PersonMessage();
            personMessage.Timestamp = DateTime.Now;
            personMessage.Person = new Person();
            personMessage.Person.Id = id;

            service.removeFamilyMember(personMessage);
        }
    }
}