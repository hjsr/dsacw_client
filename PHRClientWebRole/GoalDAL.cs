﻿using System;
using PHRClientWebRole.PHRGoalService;
using System.Web.Security;
using System.Collections.Generic;

namespace PHRClientWebRole
{
    public class GoalDAL
    {
        public static Goal[] selectAllGoals()
        {
            GoalServiceClient client = new GoalServiceClient();
            IGoalService service = client.ChannelFactory.CreateChannel();

            IdMessage idUser = new IdMessage();
            idUser.Timestamp = DateTime.Now;
            idUser.Id = (int)Membership.GetUser().ProviderUserKey;

            GoalArrayMessage goalArrayMessage = service.getAllGoals(idUser);

            return goalArrayMessage.GoalArray;
        }

        public static void updateGoal(int id, string caption, string description, int currentProgress)
        {
            GoalServiceClient client = new GoalServiceClient();
            IGoalService service = client.ChannelFactory.CreateChannel();

            GoalMessage goalMessage = new GoalMessage();
            goalMessage.Timestamp = DateTime.Now;
            goalMessage.Goal = new Goal();
            goalMessage.Goal.Caption = caption;
            goalMessage.Goal.CurrentProgress = currentProgress;
            goalMessage.Goal.Description = description;
            goalMessage.Goal.Id = id;

            service.editGoal(goalMessage);
        }

        public static void insertGoal(string caption, string description)
        {
            GoalServiceClient client = new GoalServiceClient();
            IGoalService service = client.ChannelFactory.CreateChannel();

            GoalMessage goalMessage = new GoalMessage();
            goalMessage.Timestamp = DateTime.Now;
            goalMessage.Goal = new Goal();
            goalMessage.Goal.Caption = caption;
            goalMessage.Goal.Description = description;
            goalMessage.Goal.IdUser = (int)Membership.GetUser().ProviderUserKey;

            service.addGoal(goalMessage);
        }

        public static void deleteGoal(int id)
        {
            GoalServiceClient client = new GoalServiceClient();
            IGoalService service = client.ChannelFactory.CreateChannel();

            GoalMessage goalMessage = new GoalMessage();
            goalMessage.Timestamp = DateTime.Now;
            goalMessage.Goal = new Goal();
            goalMessage.Goal.Id = id;

            service.removeGoal(goalMessage);
        }

        public static Dictionary<DateTime, int> selectGoalAllProgress(int id)
        {
            GoalServiceClient client = new GoalServiceClient();
            IGoalService service = client.ChannelFactory.CreateChannel();

            GoalMessage goalMessage = new GoalMessage();
            goalMessage.Timestamp = DateTime.Now;
            goalMessage.Goal = new Goal();
            goalMessage.Goal.Id = id;

            ProgressMapMessage progressMapMessage = service.getAllProgress(goalMessage);

            return progressMapMessage.ProgressMap;
        }
    }
}