﻿using System;
using System.Web.Security;
using PHRClientWebRole.PHRHealthRecordService;

namespace PHRClientWebRole
{
    public class HealthRecordDAL
    {
        public static HealthRecord[] selectAllHealthRecords(int id)
        {
            HealthRecordServiceClient client = new HealthRecordServiceClient();
            IHealthRecordService service = client.ChannelFactory.CreateChannel();

            IdMessage idUser = new IdMessage();
            idUser.Timestamp = DateTime.Now;
            idUser.Id = id;

            HealthRecordArrayMessage healthRecordArrayMessage = service.getAllHealthRecords(idUser);

            return healthRecordArrayMessage.HealthRecordArray;
        }

        public static void insertHealthRecord(string caption, int idPerson, HealthRecordType type)
        {
            HealthRecordServiceClient client = new HealthRecordServiceClient();
            IHealthRecordService service = client.ChannelFactory.CreateChannel();

            HealthRecordMessage healthRecordMessage = new HealthRecordMessage();
            healthRecordMessage.Timestamp = DateTime.Now;
            healthRecordMessage.HealthRecord = new HealthRecord();
            healthRecordMessage.HealthRecord.Caption = caption;
            healthRecordMessage.HealthRecord.IdPerson = idPerson;
            healthRecordMessage.HealthRecord.Type = type;

            service.addHealthRecord(healthRecordMessage);
        }

        public static void deleteHealthRecord(int id)
        {
            HealthRecordServiceClient client = new HealthRecordServiceClient();
            IHealthRecordService service = client.ChannelFactory.CreateChannel();

            HealthRecordMessage healthRecordMessage = new HealthRecordMessage();
            healthRecordMessage.Timestamp = DateTime.Now;
            healthRecordMessage.HealthRecord = new HealthRecord();
            healthRecordMessage.HealthRecord.Id = id;

            service.removeHealthRecord(healthRecordMessage);
        }
    }
}