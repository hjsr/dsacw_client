﻿<%@ Page Title="Health Records" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" 
    CodeBehind="HealthRecords.aspx.cs" Inherits="PHRClientWebRole._HealthRecords" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>
        Health Records of
        <asp:DropDownList ID="DropDownListFamilyMembers" runat="server" 
            DataSourceID="FamilyMemberDataSource" DataTextField="Name" 
            DataValueField="Id" AutoPostBack="True" 
            onselectedindexchanged="DropDownListFamilyMembers_SelectedIndexChanged">
        </asp:DropDownList>
        <asp:ObjectDataSource ID="FamilyMemberDataSource" runat="server" 
            SelectMethod="selectAllFamilyMembers" 
            TypeName="PHRClientWebRole.FamilyMemberDAL"></asp:ObjectDataSource>
    </h2>
    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" 
        CellPadding="4" DataSourceID="HealthRecordDataSource" 
        ForeColor="#333333" GridLines="None" DataKeyNames="Id">
        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
        <Columns>
            <asp:CommandField ShowDeleteButton="True" />
            <asp:BoundField DataField="Type" HeaderText="Type" 
                SortExpression="Type" />
            <asp:BoundField DataField="Caption" HeaderText="Caption" 
                SortExpression="Caption" />
        </Columns>
        <EditRowStyle BackColor="#999999" />
        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
        <SortedAscendingCellStyle BackColor="#E9E7E2" />
        <SortedAscendingHeaderStyle BackColor="#506C8C" />
        <SortedDescendingCellStyle BackColor="#FFFDF8" />
        <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
    </asp:GridView>
    <asp:LinkButton ID="LinkButton1" runat="server" onclick="LinkButton1_Click">Insert</asp:LinkButton>
    <asp:DetailsView ID="DetailsView1" runat="server" CellPadding="4" 
        DataSourceID="HealthRecordDataSource" ForeColor="#333333" GridLines="None" 
        Height="50px" Width="125px"
         OnItemCommand="DetailsView1_ItemCommand" AutoGenerateRows="False" 
        DefaultMode="Insert" Visible="False">
        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
        <CommandRowStyle BackColor="#E2DED6" Font-Bold="True" />
        <EditRowStyle BackColor="#999999" />
        <FieldHeaderStyle BackColor="#E9ECF1" Font-Bold="True" />
        <Fields>
            <asp:TemplateField HeaderText="Type" SortExpression="Type">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Type") %>'></asp:TextBox>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:DropDownList ID="DropDownList2" runat="server" 
                        SelectedValue='<%# Bind("Type") %>'>
                        <asp:ListItem Value="0">ALLERGIE</asp:ListItem>
                        <asp:ListItem Value="1">BLOOD_PRESSURE</asp:ListItem>
                        <asp:ListItem Value="2">FAMILY_HISTORY</asp:ListItem>
                        <asp:ListItem Value="3">ILLNESS</asp:ListItem>
                        <asp:ListItem Value="4">IMAGE</asp:ListItem>
                        <asp:ListItem Value="5">IMMUNIZATION</asp:ListItem>
                        <asp:ListItem Value="6">LAB_RESULT</asp:ListItem>
                        <asp:ListItem Value="7">MEDICATION</asp:ListItem>
                    </asp:DropDownList>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("Type") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="Caption" HeaderText="Caption" 
                SortExpression="Caption" />
            <asp:TemplateField HeaderText="IdPerson" SortExpression="IdPerson">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("IdPerson") %>'></asp:TextBox>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:DropDownList ID="DropDownList1" runat="server" 
                        DataSourceID="FamilyMemberDataSource" DataTextField="Name" DataValueField="Id" 
                        SelectedValue='<%# Bind("IdPerson") %>'>
                    </asp:DropDownList>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("IdPerson") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:CommandField ShowInsertButton="True" />
        </Fields>
        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
    </asp:DetailsView>
    <asp:ObjectDataSource ID="HealthRecordDataSource" runat="server" 
        DeleteMethod="deleteHealthRecord" InsertMethod="insertHealthRecord" 
        SelectMethod="selectAllHealthRecords" 
        TypeName="PHRClientWebRole.HealthRecordDAL">
        <DeleteParameters>
            <asp:Parameter Name="id" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="caption" Type="String" />
            <asp:Parameter Name="idPerson" Type="Int32" />
            <asp:Parameter Name="type" Type="Object" />
        </InsertParameters>
        <SelectParameters>
            <asp:ControlParameter ControlID="DropDownListFamilyMembers" Name="id" 
                PropertyName="SelectedValue" Type="Int32" />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>
