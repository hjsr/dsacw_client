﻿using System;
using System.Web;
using System.Web.UI.WebControls;
using PHRClientWebRole.PHRLoginService;

namespace PHRClientWebRole.Account
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            RegisterHyperLink.NavigateUrl = "Register.aspx?ReturnUrl=" + HttpUtility.UrlEncode(Request.QueryString["ReturnUrl"]);
        }

        protected void LoginUser_Authenticate(object sender, AuthenticateEventArgs e)
        {
            bool Authenticated = false;
            Authenticated = SiteSpecificAuthenticationMethod(LoginUser.UserName, LoginUser.Password);

            e.Authenticated = Authenticated;
        }

        private bool SiteSpecificAuthenticationMethod(string UserName, string Password)
        {
            LoginServiceClient client = new LoginServiceClient();
            ILoginService service = client.ChannelFactory.CreateChannel();

            UserMessage request = new UserMessage();
            request.Timestamp = DateTime.Now;
            request.User = new User();
            request.User.Username = UserName;
            request.User.Password = Password;

            IdMessage response = service.login(request);

            return response.Id != -1;
        }
    }
}
